angular.module('starter')
    .factory('RequestService', function ($http) {

        var requestServiceFactory = {};

        function json_merge_recursive(json1, json2) {
            var out = {};
            for (var k1 in json1) {
                if (json1.hasOwnProperty(k1)) out[k1] = json1[k1];
            }
            for (var k2 in json2) {
                if (json2.hasOwnProperty(k2)) {
                    if (!out.hasOwnProperty(k2)) out[k2] = json2[k2];
                    else if (
                        (typeof out[k2] === 'object') && (out[k2].constructor === Object) &&
                        (typeof json2[k2] === 'object') && (json2[k2].constructor === Object)
                    ) out[k2] = json_merge_recursive(out[k2], json2[k2]);
                }
            }
            return out;
        }

        var _get = function (url, params) {
            //console.log(params);
            var applicationKey = {
                "k": "AR78UK9Q"
            };
            var paramsWithKey = json_merge_recursive(params,applicationKey)
                
            //console.log(paramsWithKey);
            return $http({
                url: "http://ws.kino.kz/" + url,
                method: "GET",
                params: paramsWithKey
            }).
            then(function (response) {
                if( response.status == 200 ) {
                    console.log("Данные получены от: " + url);
                    console.log(response);
                    return response.data;
                } else {
                    console.log("Ошибка при получении данных");
                    return response.error;
                }
                
                


            });
            

        }

        requestServiceFactory.get = _get;

        return requestServiceFactory;

    });


/*****************************************************/
/*   HELPER FACTORIES HERE ==========>>>>           */
/***************************************************/    
angular.module('ionic.utils', [])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);