angular.module('starter.controllers')
  .controller('CinemaScheduleCtrl', function ($scope, $stateParams, $localstorage, $filter, RequestService, $ionicPopover, $state) {
    $scope.Cinema = $stateParams.cinemaData;
    $scope.daytext = "сегодня";
    var cityId = $localstorage.getObject('city').id;
    var day = $stateParams.day;


    var params = {
      cinema: $stateParams.cinemaID,
      day: $stateParams.day,
      sort: 0,
      city: $localstorage.getObject('city')['id']

    };


    //console.log('CinemaDATA', $scope.Cinema)

    RequestService.get('schedule_cinema.data', params).then(function (result) {

      $scope.CinemaSessions = result.data.times;
      $scope.TodayDate = result.data.date;
      console.log("Cinema Sessions:", $scope.CinemaSessions);
    });
    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function (popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function ($event) {
      $scope.popover.show($event);
    };
    $scope.closePopover = function () {
      $scope.popover.hide();
    };
    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function () {
      $scope.popover.remove();
    });
    // Execute action on hide popover
    $scope.$on('popover.hidden', function () {
      // Execute action
    });
    // Execute action on remove popover
    $scope.$on('popover.removed', function () {
      // Execute action
    });

    $scope.share = function (moviename, time, cinema, children, student, adult) {
      $state.go('app.smsform', {
        time: time,
        movie: moviename,
        cinema: cinema,
        child: children,
        stud: student,
        adult: adult
      });

    };

    $scope.showSessionsforDay = function (dayId) {
      console.log('show another session');

      params = {
        cinema: $stateParams.cinemaID,
        day: dayId,
        sort: 0,
        city: $localstorage.getObject('city')['id']

      };

      if (dayId == 0)
        $scope.daytext = "сегодня";
      else if (dayId == 1) {
        $scope.daytext = "завтра";
      }
      else if (dayId == 2) {
        $scope.daytext = "послезавтра";
      }

      RequestService.get('schedule_cinema.data', params).then(function (result) {
        $scope.CinemaSessions = result.data.times;
        $scope.TodayDate = result.data.date;
        console.log("Cinema Sessions:", $scope.CinemaSessions);

      });


    };

  })
