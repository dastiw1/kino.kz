angular.module('starter.controllers')
  .controller('MoviesCtrl', function ($scope, $ionicPopup, $stateParams, $ionicTabsDelegate, $state, $timeout, $http, $timeout, RequestService, $localstorage) {
    var cityId = $localstorage.getObject('city').id;
    var day = $stateParams.day;
    var params = {
      city: cityId,
      day: day

    };

    $scope.showAlert = function () {
      var alertPopup = $ionicPopup.alert({
        title: 'Киносеансы не найдены',
        template: 'Возможно расписание сеансов нами еще не получены или неполадки с сетью'
      });

      alertPopup.then(function (res) {
        $state.go('app.main')


      });
    };



    RequestService.get('movies.data', params).then(function (result) {

      if( result.data.length == 0  ){
        $scope.showAlert()

      }
      $scope.movies = result.data;

      //console.log($scope.premieres);
    });


  })
