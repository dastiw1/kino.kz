angular.module('starter.controllers')
    .controller('PremiereCtrl', function ($scope, $stateParams, $http, $timeout, RequestService, $localstorage) {
        var cityId = $localstorage.getObject('city').id;
        var params = {
            city:cityId,
            type:2

        }
        RequestService.get('premiere.data', params).then(function(result){

            $scope.premieres = result.data;

            //console.log($scope.premieres);
        });


})
